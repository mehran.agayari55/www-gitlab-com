---
layout: markdown_page
title: "Category Vision - Container Registry"
---

- TOC
{:toc}

## Container Registry

GitLab Container Registry is a secure and private registry for Docker images. Built on open source software, GitLab Container Registry isn't just a standalone registry; it's completely integrated with GitLab.  Easily use your images for GitLab CI, create images specific for tags or branches and much more.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Container%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

## What's Next & Why

TODO

## Competitive Landscape

TODO

## Top Customer Success/Sales Issue(s)

TODO

## Top Customer Issue(s)

TODO

## Top Internal Customer Issue(s)

TODO

## Top Vision Item(s)

TODO
