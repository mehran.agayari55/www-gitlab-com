---
layout: markdown_page
title: "Category Vision - Gitter"
---

- TOC
{:toc}

## Gitter

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Gitter is an open source instant messaging application for developers, and is the place to connect the open source and software development community.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Gitter)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=Gitter)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**In progress: [Threaded conversations](https://gitlab.com/groups/gitlab-org/-/epics/360)** are the number one request from existing users and are needed in busy rooms so that multiple conversations can happen simultaneously. This feature is critical for retaining existing customers.

**Next up: [GitLab communities and rooms](https://gitlab.com/groups/gitlab-org/-/epics/398)** are needed to grow the user base of Gitter by making it work well with GitLab, which includes making it easy for every project to have a Gitter community.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Slack](https://slack.com)
- [Mattermost](https://mattermost.com/)
- [Spectrum](https://spectrum.chat/)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

