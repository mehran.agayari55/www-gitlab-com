---
layout: markdown_page
title: "Category Vision - Live Coding"
---

- TOC
{:toc}

## CATEGORY_NAME

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Live shared programming environments allow pair programming and collaboration between developers in different locations with greater ease.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=live%20coding)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=live%20coding)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**Next: [Broadcast Web IDE](https://gitlab.com/groups/gitlab-org/-/epics/694)**

Read only broadcasting from the Web IDE will allow the author of a merge request to talk another developer through a change using the Web IDE (audio via Zoom/Hangouts), and allow the reviewer to jump to other parts of the code if they want to dig deeper.

Broadcasting should be a simpler problem because changes are only flowing in a single direction, removing the problem of conflicts. However, using operational transformations, we should be able to quickly follow up with a second iteration that allows multiple people to edit simultaneously.

There are also other interesting applications of broadcasting, like in the classroom allowing everyone to follow along on a lecture/demo taught from the editor.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Microsoft Live Share](https://www.visualstudio.com/services/live-share/)
- [Codesandbox.io](https://codesandbox.io/docs/live) – [Announcement](https://hackernoon.com/introducing-codesandbox-live-real-time-code-collaboration-in-the-browser-6d508cfc70c9)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

