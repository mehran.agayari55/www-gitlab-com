---
layout: markdown_page
title: "Category Vision - Gitaly"
---

- TOC
  {:toc}

## Gitaly

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

Gitaly is a Git RPC service for handling all the Git call made by GitLab.

- See a high-level [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Gitaly&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Gitaly)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com) /
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience

<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**In progress (ETA 11.11):** [Create forks with deduplicated objects](https://gitlab.com/groups/gitlab-org/-/epics/189)

Important to customers (incl Drupal) who self host GitLab, typically on their own hardware, where forking workflows can cause massive disk usage and require hardware upgrades to support the storage requirements.

**In progress (ETA 12.0):** [High Availability Gitaly (Beta)](https://gitlab.com/groups/gitlab-org/-/epics/289)

Currently there is no way to run GitLab in a HA configuration without NFS. This is preventing GitLab from being in the AWS marketplace and from running GitLab in a HA configuration in Kubernetes. The first phase will be a beta focused on data replication, but without fail over workflows or monitoring node health.

**Next (soonest 12.6):** [High Availability Gitaly (General Availability)](https://gitlab.com/groups/gitlab-org/-/epics/843)

Currently there is no way to run GitLab in a HA configuration without NFS. This is preventing GitLab from being in the AWS marketplace and from running GitLab in a HA configuration in Kubernetes. The second will make all Gitaly components HA, support automatic fail over, transactional commits, and basic management/monitoring tools.

## Competitive Landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [GitHub.com](https://github.com)
- [GitHub Enterprise](https://github.com/enterprise)
- [Bitbucket Cloud](https://bitbucket.org/product/)
- [Bitbucket Server](https://bitbucket.org/product/)
- [Perforce](https://perforce.com)
- [CVS: Concurrent Versions System](https://nongnu.org/cvs/)
- [SVN: Apache Subversion](https://subversion.apache.org/)

## Market Research

<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity

<!-- This section should highlight the business opportunity highlighted by the particular category. -->

Git is the market leading version control system, used by the majority of individuals and enterprises. According to the [2018 Stack Overflow Developer Survey](https://insights.stackoverflow.com/survey/2018/#work-_-version-control) over 88% of respondents use Git.

According to a [2016 Bitrise survey](https://blog.bitrise.io/state-of-app-development-2016#self-hosted), 62% of apps hosted by SaaS provider were hosted in GitHub, and 95% of apps are hosted in by a SaaS provider.

## Analyst Landscape

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

- **[Git: Native Support for Large Files](https://gitlab.com/groups/gitlab-org/-/epics/958)** is very important to companies that need to version large binary assets, like game studios. These companies primarily use Perforce because Git LFS provides poor experience with complex commands and careful workflows needed to avoid large files entering the repository. GitLab has been supporting work to provide a more native large file workflow based on promiser packfiles which will be very significant to analysts and customers when the feature is ready.

## Top Customer Success/Sales issue(s)

<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- **[High Availability Gitaly](https://gitlab.com/groups/gitlab-org/-/epics/843)** is need to allow customers to avoid needing NFS to achieve a highly available GitLab instance. The network latency of any network based file system, like NFS, EFS, Gluster, will negatively impact Git performance because of Git's disk access requirements. It is important to customers want to run an HA GitLab instance that we provide a better way.
- **[Gitaly N+1 issues](https://gitlab.com/groups/gitlab-org/-/epics/827)** are a bad Git access pattern that results in bad performance of the GitLab application. Gitaly needs to work with each time to replace these bad implementations.
- **[Git: Partial Clone](https://gitlab.com/groups/gitlab-org/-/epics/915)** is important to many of our largest customer who have very large projects hosted in Perforce, SVN or CVS. Because Git was designed to support distributed workflows, each copy of the repository includes the entire history of the project. The ongoing work to implement support for partial clone in Git and in GitLab is how we will provide a pathway to migrate these large projects to GitLab.

## Top user issue(s)

<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)

<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- **[High Availability Gitaly](https://gitlab.com/groups/gitlab-org/-/epics/843)** is important to the Distribution team so that we can offer a GitLab Helm chart that supports high availability. It is also important to the Production team so that we can consider deploying GitLab.com in Kubernetes.

## Top Vision Item(s)

<!-- What's the most important thing to move your vision forward?-->

- [Git at scale](https://gitlab.com/groups/gitlab-org/-/epics/773)
- [Git: Native Support for Large Files](https://gitlab.com/groups/gitlab-org/-/epics/958)
- [Git: Partial Clone](https://gitlab.com/groups/gitlab-org/-/epics/915)
