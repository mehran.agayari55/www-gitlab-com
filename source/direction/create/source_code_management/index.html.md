---
layout: markdown_page
title: "Category Vision - Source Code Management"
---

- TOC
{:toc}

## Source Code Management

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=repository)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=repository)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**Next (11.11 – 12.0): [One-click merge of multiple merge requests](https://gitlab.com/groups/gitlab-org/-/epics/881)**

This is the first iteration of **Cross project merge requests** (née "group merge requests"). Improving cross project workflows is critical to customers with complex project structures that span multiple projects.

**Next (11.11): [Private forks for confidential merge requests](https://gitlab.com/groups/gitlab-org/-/epics/753)

Public projects need to be able to resolve security issues in private so that the vulnerability isn't leaked during the development process. The MVC is to support workflows where there is a public project, and a private fork.

**Next: [Forking improvements](https://gitlab.com/groups/gitlab-org/-/epics/264)**

Forking workflows are important for open source projects on public instances like GitLab.com, but they are also used for private projects on GitLab.com and elsewhere. There are a range of significant shortcomings in the forking workflow that should be resolved.

Forking workflows should be fully supported in GitLab so that they can be used by open source projects and enterprises, public or private.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [GitHub.com](https://github.com)
- [GitHub Enterprise](https://github.com/enterprise)
- [Bitbucket Cloud](https://bitbucket.org/product/)
- [Bitbucket Server](https://bitbucket.org/product/)
- [Perforce](https://perforce.com)
- [CVS: Concurrent Versions System](https://nongnu.org/cvs/)
- [SVN: Apache Subversion](https://subversion.apache.org/)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

- Large file support (see [Gitaly direction](/direction/create/gitaly))

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Reduce merge request contention for fast-forward merge](https://gitlab.com/groups/gitlab-org/-/epics/521)
- [Best practice forking](https://gitlab.com/groups/gitlab-org/-/epics/868) - Some organizations/people that use forking have requested automatic mirroring of the upstream project to the downstream fork. This problem is better solved by helping people use forks more efficiently. We can do this with tips and Git output to help people succeed.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Clone statistics](https://gitlab.com/gitlab-org/gitlab-ce/issues/21743)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- [Confidential merge requests](https://gitlab.com/groups/gitlab-org/-/epics/753)

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Distributed merge requests](https://gitlab.com/groups/gitlab-org/-/epics/260)
