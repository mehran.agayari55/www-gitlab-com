---
layout: markdown_page
title: Handbook
twitter_image: '/images/tweets/handbook-gitlab.png'
---

The GitLab team handbook is the central repository for how we run the company. Printed, it consists of over [2,000 pages of text](/handbook/tools-and-tips/#count-handbook-pages). As part of our value of being transparent the handbook is <a href="https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook">open to the world</a>, and we welcome feedback<a name="feedback"></a>. Please make a <a href="https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests">merge request</a> to suggest improvements or add clarifications.
Please use <a href="https://gitlab.com/gitlab-com/www-gitlab-com/issues">issues</a> to ask questions.

* [General](/handbook/)
  * [Values](/handbook/values)
  * [General Guidelines](/handbook/general-guidelines)
  * [Handbook usage](/handbook/handbook-usage)
  * [Tools and tips](/handbook/tools-and-tips)
  * [Security Practices](/handbook/security)
  * [Using Git to update this website](/handbook/git-page-update)
* [People Operations](/handbook/people-operations)
  * [Communication](/handbook/communication)
    * [Live streaming](/handbook/live-streaming)
    * [Inclusion & Diversity - Being an ally](/handbook/ally-resources)
  * [Code of Conduct](/handbook/people-operations/code-of-conduct/)
    * [Anti-Harassment Policy](/handbook/anti-harassment)
  * [Leadership](/handbook/leadership)
  * [Hiring](/handbook/hiring)
    * [Greenhouse](/handbook/hiring/greenhouse)
    * [Vacancies](/handbook/hiring/vacancies)
    * [Interviewing](/handbook/hiring/interviewing)
    * [Jobs FAQ](/jobs/faq)
  * [Onboarding](/handbook/general-onboarding)
  * [Benefits](/handbook/benefits)
    * [Incentives](/handbook/incentives)
    * [Paid time off](/handbook/paid-time-off)
  * [Offboarding](/handbook/offboarding)
  * [Spending Company Money](/handbook/spending-company-money)
    * [Travel](/handbook/travel)
    * [Visas](/handbook/people-operations/visas/)
  * [Secret Snowflake](/handbook/people-operations/secret-snowflake)
* [Engineering](/handbook/engineering/)
  * [Development](/handbook/engineering/development/)
    * [Fellow](/handbook/engineering/development/fellow/)
    * [CI/CD Department](/handbook/engineering/development/ci-cd/)
      * [Verify](/handbook/engineering/development/ci-cd/verify/)
      * [Release](/handbook/engineering/development/ci-cd/release/)
      * [Front End Verify & Release](/handbook/engineering/development/ci-cd/fe-verify-release/)
      * [Package](/handbook/engineering/development/ci-cd/package/)
    * [Dev Department](/handbook/engineering/development/dev/)
      * [Manage Team](/handbook/engineering/development/dev/manage/)
      * [Front End Manage Team](/handbook/engineering/development/dev/fe-manage/)
      * [Create Team](/handbook/engineering/development/dev/create/)
      * [Front End Create Team](/handbook/engineering/development/dev/fe-create/)
      * [Plan Team](/handbook/engineering/development/dev/plan/)
      * [Front End Plan Team](/handbook/engineering/development/dev/fe-plan/)
      * [Gitter Team](/handbook/engineering/development/dev/gitter/)
    * [Enablement Department](/handbook/engineering/development/enablement/)
      * [Geo Team](/handbook/engineering/development/enablement/geo/)
      * [Gitaly Team](/handbook/engineering/development/enablement/gitaly/)
      * [Distribution Team](/handbook/engineering/development/enablement/distribution/)
      * [Memory Team](/handbook/engineering/development/enablement/memory/)
      * [Ecosystem Team](/handbook/engineering/development/enablement/ecosystem/)
      * [Fulfillment Team](/handbook/engineering/development/enablement/fulfillment/)
      * [Front End Fulfillment Team](/handbook/engineering/development/enablement/fe-fulfillment/)
    * [Growth Department](/handbook/engineering/development/growth/)
      * [Activation Team](/handbook/engineering/development/growth/activation/)
      * [Adoption Team](/handbook/engineering/development/growth/adoption/)      
      * [Upsell Team](/handbook/engineering/development/growth/upsell/)
      * [Retention Team](/handbook/engineering/development/growth/retention/)
    * [Ops Department](/handbook/engineering/development/ops/)
      * [Monitor Team](/handbook/engineering/development/ops/monitor/)
      * [FE Monitor Team](/handbook/engineering/development/ops/fe-monitor/)
      * [Configure Team](/handbook/engineering/development/ops/configure/)
      * [Serverless Team](/handbook/engineering/development/ops/serverless/)
      * [Front End Configure & Serverless Team](/handbook/engineering/development/ops/fe-configure-serverless/)
    * [Secure Department](/handbook/engineering/development/secure/)
      * [Secure Team](/handbook/engineering/development/secure/secure/)
      * [FE Secure Team](/handbook/engineering/development/secure/fe-secure/)
      * [Defend Team](/handbook/engineering/development/secure/defend/)
  * [Dev Backend Department](/handbook/engineering/dev-backend/)
    * [Create Team](/handbook/engineering/dev-backend/create/)
    * [Distribution Team](/handbook/engineering/dev-backend/distribution/)
    * [Geo Team](/handbook/engineering/dev-backend/geo/)
    * [Gitaly Team](/handbook/engineering/dev-backend/gitaly/)
    * [Gitter Team](/handbook/engineering/dev-backend/gitter/)
    * [Manage Team](/handbook/engineering/dev-backend/manage/)
    * [Plan Team](/handbook/engineering/dev-backend/plan/)
  * [Frontend Department](/handbook/engineering/frontend/)
    * [CI/CD](/handbook/engineering/frontend/ci-cd/)
    * [Configure](/handbook/engineering/frontend/configure/)
    * [Create](/handbook/engineering/frontend/create/)
    * [Manage](/handbook/engineering/frontend/manage/)
    * [Monitor](/handbook/engineering/frontend/monitor/)
    * [Plan](/handbook/engineering/frontend/plan/)
    * [Secure](/handbook/engineering/frontend/secure/)
  * [Infrastructure Department](/handbook/engineering/infrastructure/)
    * [Database Team](/handbook/engineering/infrastructure/database/)
    * [Production Team](/handbook/engineering/infrastructure/production/)
  * [Ops Department](/handbook/engineering/ops/)
    * [Configure Team](/handbook/engineering/ops/configure/)
    * [Monitor Team](/handbook/engineering/ops/monitor/)
    * [Serverless](/handbook/engineering/ops/serverless/)
    * [Release Team](/handbook/engineering/ops/release/)
    * [Secure Team](/handbook/engineering/ops/secure/)
    * [Verify Team](/handbook/engineering/ops/verify/)
  * [Quality Department](/handbook/engineering/quality/)
  * [Security Department](/handbook/engineering/security/)
  * [Support Department](/handbook/support/)
    * [Incident Management for Self-Managed Customers](/handbook/support/incident-management/)
  * [UX Department](/handbook/engineering/ux/)
* [Marketing](/handbook/marketing)
  * [Website](/handbook/marketing/website/)
  * [Blog](/handbook/marketing/blog)
  * [Social Media Guidelines](/handbook/marketing/social-media-guidelines)
  * [Marketing and Sales Development](/handbook/marketing/marketing-sales-development/)
    * [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
    * [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
    * [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)
    * [Marketing Programs](/handbook/marketing/marketing-sales-development/marketing-programs/)
    * [Online Marketing](/handbook/marketing/marketing-sales-development/online-marketing/)
  * [Corporate Marketing](/handbook/marketing/corporate-marketing/)
      * [Content Marketing](/handbook/marketing/corporate-marketing/content/)
  * [Community Relations](/handbook/marketing/community-relations)
  * [Product Marketing](/handbook/marketing/product-marketing/)
    * [Demos](/handbook/marketing/product-marketing/demo/)
  * [Marketing Career Development](/handbook/marketing/career-development/)
* [Sales](/handbook/sales)
  * [Account Management](/handbook/account-management)
  * [Customer Success](/handbook/customer-success/)
  * [Reseller Channels](/handbook/resellers/)
  * Sales Operations - moved to [Business Operations](/handbook/business-ops)
  * [Reporting](/handbook/business-ops/reporting)
* [Finance](/handbook/finance)
  * [Stock Options](/handbook/stock-options)
  * [Board meetings](/handbook/board-meetings)
  * [Business Operations](/handbook/business-ops)
    * [Data Team](/handbook/business-ops/data-team/)
* [Product](/handbook/product)
  * [Release posts](/handbook/marketing/blog/release-posts/)
  * [Making Gifs](/handbook/product/making-gifs)
  * [Data analysis](/handbook/business-ops/data-team/#-data-analysis-process)
  * [Technical Writing](/handbook/product/technical-writing/)
  * [Markdown Guide](/handbook/product/technical-writing/markdown-guide/)
* [Legal](/handbook/legal)
  * [DMCA](/handbook/dmca/)
  * [Signing legal documents](/handbook/signing-legal-documents)
* [Alliances](/handbook/alliances)
* [Changelog](/handbook/CHANGELOG.html)

<style>
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
</style>
