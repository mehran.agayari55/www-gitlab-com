---
layout: markdown_page
title: "TPM.3.01 - Approved Service Provider Listing Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TPM.3.01 - Approved Service Provider Listing

## Control Statement

GitLab maintains a list of approved, managed service providers and the services they provide to GitLab.

## Context

Maintaining a list of approved service providers will assist in validating exactly what a service provider offers.  Documentation should include:

* Provided service(s)
* What, if any, data is shared
* Data encryption utilized
* Date of last PCI compliance

## Scope

All externally sourced service providers utilized by GitLab that handles credit card data.

## Ownership

Owner of service provider relationship.

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.3.01_approved_service_provider_listing.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.3.01_approved_service_provider_listing.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.3.01_approved_service_provider_listing.md).

## Framework Mapping

* PCI
  * 12.8.1
